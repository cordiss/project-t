﻿namespace ProjectT.DLL.Common.Constants
{
    public partial class Constants
    {
        public const string DefaultConnectionName = "DefaultConnection";
        public const string DefaultUserProfilePhotoPath = @"./resources/catprofile96px.png";
    }
}