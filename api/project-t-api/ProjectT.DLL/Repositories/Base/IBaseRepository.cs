﻿using ProjectT.DLL.Entities.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectT.DLL.Repositories.Base
{
    public interface IBaseRepository<TEntity> 
        where TEntity : BaseEntity
    {
        public Task<IEnumerable<TEntity>> GetAllAsync();
        public Task<TEntity> GetByIdAsync(long id);
        public Task<TEntity> CreateAsync(TEntity entity);
        public Task<TEntity> UpdateAsync(TEntity entity);
        public Task<int> DeleteAsync(TEntity entity);
    }
}