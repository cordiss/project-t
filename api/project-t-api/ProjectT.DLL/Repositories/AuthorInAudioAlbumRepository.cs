﻿using Microsoft.EntityFrameworkCore;
using ProjectT.DLL.AppContext;
using ProjectT.DLL.Entities;
using ProjectT.DLL.Models;
using ProjectT.DLL.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectT.DLL.Repositories
{
    public class AuthorInAudioAlbumRepository : IAuthorInAudioAlbumRepository
    {
        private readonly ApplicationContext _context;

        public AuthorInAudioAlbumRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<ResponseModel<Audio>> GetAudiosAsync()
        {
            var responseModel = new ResponseModel<Audio>();

            var result = _context.AuthorInAudioAlbums
                .Select(x => new Audio
                {
                    Id = x.AudioId,
                    Title = x.Audio.Title,
                    TrackNumber = x.Audio.TrackNumber,
                    Year = x.Audio.Year,
                    File = x.Audio.File,
                    CreationData = x.Audio.CreationData,
                    IsRemoved = x.Audio.IsRemoved,

                    Author = x.Author,
                    Album = x.Album
                });

            var itemsAmong = await result.CountAsync();

            responseModel.Entities = await result.ToListAsync();
            responseModel.ItemsCount = itemsAmong;
                
            return responseModel;
        }

        public async Task<Audio> GetAudioByIdAsync(long id)
        {
            var result = await _context.AuthorInAudioAlbums
                .Select(x => new Audio
                {
                    Id = x.AudioId,
                    Title = x.Audio.Title,
                    TrackNumber = x.Audio.TrackNumber,
                    Year = x.Audio.Year,
                    File = x.Audio.File,
                    CreationData = x.Audio.CreationData,
                    IsRemoved = x.Audio.IsRemoved,

                    Author = x.Author,
                    Album = x.Album
                })
                .FirstOrDefaultAsync(x => x.Id == id);

            return result;
        }

        public async Task<ResponseModel<Author>> GetAuthorsAsync()
        {
            var responseModel = new ResponseModel<Author>();

            var result = _context.AuthorInAudioAlbums
                .Include(x => x.Author)
                .Include(x => x.Album)
                .Include(x => x.Audio)
                .AsEnumerable()
                .GroupBy(x => x.AuthorId)
                .Select(group => new Author
                {
                    Id = group.Select(x => x.AuthorId).FirstOrDefault(),
                    Name = group.Select(x => x.Author.Name).FirstOrDefault(),
                    Description = group.Select(x => x.Author.Description).FirstOrDefault(),
                    CreationData = group.Select(x => x.Author.CreationData).FirstOrDefault(),
                    IsRemoved = group.Select(x => x.Author.IsRemoved).FirstOrDefault(),

                    Albums = group.Select(x => x.Album).ToList(),
                    Audios = group.Select(x => x.Audio).ToList()
                });

            var itemsAmong = result.Count();

            responseModel.Entities = result;
            responseModel.ItemsCount = itemsAmong;
            return responseModel;
        }

        public async Task<Author> GetAuthorByIdAsync(long id)
        {
            var result = _context.AuthorInAudioAlbums
                .Include(x => x.Author)
                .Include(x => x.Album)
                .Include(x => x.Audio)
                .AsEnumerable()
                .GroupBy(x => x.AuthorId)
                .Select(group => new Author
                {
                    Id = group.Select(x => x.AuthorId).FirstOrDefault(),
                    Name = group.Select(x => x.Author.Name).FirstOrDefault(),
                    Description = group.Select(x => x.Author.Description).FirstOrDefault(),
                    CreationData = group.Select(x => x.Author.CreationData).FirstOrDefault(),
                    IsRemoved = group.Select(x => x.Author.IsRemoved).FirstOrDefault(),

                    Albums = group.Select(x => x.Album).ToList(),
                    Audios = group.Select(x => x.Audio).ToList()
                })
                .FirstOrDefault();
            return result;
        }

        public async Task<ResponseModel<Album>> GetAlbumsAsync()
        {
            var responseModel = new ResponseModel<Album>();

            var result = _context.AuthorInAudioAlbums
                .Select(x => new
                {
                    Author = x.Author,
                    Album = x.Album,
                    Audios = _context.AuthorInAudioAlbums.Select(x => x.Audio).Where(y => y.Id == x.AudioId)
                })
                .Select(a => new Album
                {
                    Id = a.Album.Id,
                    Title = a.Album.Title,
                    Year = a.Album.Year,
                    Description = a.Album.Description,
                    CoverFile = a.Album.CoverFile,

                    Author = a.Author,
                    Audios = a.Audios.ToList()
                });

            var itemsAmong = await result.CountAsync();

            var entities = await result.ToListAsync();

            responseModel.Entities = entities;
            responseModel.ItemsCount = itemsAmong;
            return responseModel;
        }

        public async Task<Album> GetAlbumByIdAsync(long id)
        {
            var result = await _context.AuthorInAudioAlbums
                .Select(x => new
                {
                    Author = x.Author,
                    Album = x.Album,
                    Audios = _context.AuthorInAudioAlbums.Select(x => x.Audio).Where(y => y.Id == x.AudioId)
                })
                .Select(a => new Album
                {
                    Id = a.Album.Id,
                    Title = a.Album.Title,
                    Year = a.Album.Year,
                    Description = a.Album.Description,
                    CoverFile = a.Album.CoverFile,

                    Author = a.Author,
                    Audios = a.Audios.ToList()
                })
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            return result;
        }
    }
}