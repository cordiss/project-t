﻿using ProjectT.DLL.Entities;
using ProjectT.DLL.Models;
using System.Threading.Tasks;

namespace ProjectT.DLL.Repositories.Interfaces
{
    public interface IAuthorInAudioAlbumRepository
    {
        Task<ResponseModel<Audio>> GetAudiosAsync();
        Task<Audio> GetAudioByIdAsync(long id);

        Task<ResponseModel<Author>> GetAuthorsAsync();
        Task<Author> GetAuthorByIdAsync(long id);

        Task<ResponseModel<Album>> GetAlbumsAsync();
        Task<Album> GetAlbumByIdAsync(long id);
    }
}