﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectT.DLL.AppContext;
using ProjectT.DLL.Common.Constants;
using ProjectT.DLL.Entities;
using ProjectT.DLL.Repositories;
using ProjectT.DLL.Repositories.Interfaces;

namespace ProjectT.DLL.Initialization
{
    public static class InitDAL
    {
        public static IConfiguration Configuration;
        public static IServiceCollection Services;

        public static void OnInit()
        {
            InitDatabaseContext();
            InitIdentity();
            InitRepositories();
        }

        private static void InitDatabaseContext()
        {
            var connection = Configuration.GetConnectionString(Constants.DefaultConnectionName);

            Services.AddDbContext<ApplicationContext>(options =>
            {
                options.UseSqlServer(connection);
            });
        }

        private static void InitIdentity()
        {
            Services.AddIdentity<User, IdentityRole>(options =>
            {
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 8;
            })
                .AddEntityFrameworkStores<ApplicationContext>()
                .AddRoleManager<RoleManager<IdentityRole>>()
                .AddDefaultTokenProviders();
        }

        private static void InitRepositories()
        {
            Services.AddScoped<IAuthorInAudioAlbumRepository, AuthorInAudioAlbumRepository>();
        }
    }   
}