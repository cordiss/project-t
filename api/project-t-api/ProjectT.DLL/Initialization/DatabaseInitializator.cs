﻿using Microsoft.AspNetCore.Identity;
using ProjectT.DLL.AppContext;
using ProjectT.DLL.Common.Constants;
using ProjectT.DLL.Entities;
using ProjectT.DLL.Entities.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectT.DLL.Initialization
{
    public static class DatabaseInitializator
    {
        public static UserManager<User> UserManager;
        public static RoleManager<IdentityRole> RoleManager;
        public static ApplicationContext Context;

        public async static Task InitializeRolesAsync()
        {
            if (await RoleManager.FindByNameAsync(EnumsClass.UserRoles.Admin.ToString()) == null)
            {
                await RoleManager.CreateAsync(new IdentityRole(EnumsClass.UserRoles.Admin.ToString()));
            }
            if (await RoleManager.FindByNameAsync(EnumsClass.UserRoles.User.ToString()) == null)
            {
                await RoleManager.CreateAsync(new IdentityRole(EnumsClass.UserRoles.User.ToString()));
            }

            if (await UserManager.FindByNameAsync("admin@gmail.com") == null)
            {
                User admin = new User
                {
                    FullName = "Great Admin",
                    Email = "admin@gmail.com",
                    UserName = "admin",
                    UserPhoto = Constants.DefaultUserProfilePhotoPath,
                    Phone = null,
                    EmailConfirmed = true,
                    CreationData = DateTime.Now,
                    UserStatus = EnumsClass.UserStatus.Active
                };
                IdentityResult result = await UserManager.CreateAsync(admin, "Tve1rRx9");
                if (result.Succeeded)
                {
                    await UserManager.AddToRoleAsync(admin, EnumsClass.UserRoles.Admin.ToString());
                }
            }
        }

        public async static Task InitializeEntitiesAsync()
        {
            InitAuthors();
            InitAlbums();
            InitAudios();
            InitAuthorInAudiosAlbums();
            InitDevices();

            await Context.SaveChangesAsync();
        }

        private static void InitAuthors()
        {
            if (!(Context.Authors.Any()))
            {
                Context.Authors.AddRange(
                    new Author
                    {
                        Name = "Metallica",
                        Description = "Trash metal band from 1982. Legend of metal music.",
                        CreationData = DateTime.Now,
                        IsRemoved = false
                    });
                Context.SaveChanges();
            }
        }

        private static void InitAlbums()
        {
            if (!(Context.Albums.Any()))
            {
                Context.Albums.AddRange(
                    new Album
                    {
                        Title = "Master Of Puppets",
                        Year = 1986,
                        Description = "Third album of Metallica",
                        Author = Context.Authors.FirstOrDefault(),
                        CreationData = DateTime.Now,
                        IsRemoved = false
                    });
                Context.SaveChanges();
            }
        }

        private static void InitAudios()
        {
            if (!(Context.Audios.Any()))
            {
                Context.Audios.AddRange(
                    new Audio
                    {
                        Title = "Master Of Puppets",
                        TrackNumber = 2,
                        Year = 1986,
                        File = null,
                        Author = Context.Authors.FirstOrDefault(),
                        Album = Context.Albums.FirstOrDefault(),
                        CreationData = DateTime.Now,
                        IsRemoved = false
                    });
                Context.SaveChanges();
            }
        }

        private static void InitAuthorInAudiosAlbums()
        {
            if (!(Context.AuthorInAudioAlbums.Any()))
            {
                Context.AuthorInAudioAlbums.AddRange(
                    new AuthorInAudioAlbum
                    {
                        AuthorId = Context.Authors.FirstOrDefault().Id,
                        AlbumId = Context.Albums.FirstOrDefault().Id,
                        AudioId = Context.Audios.FirstOrDefault().Id
                    });
            }
        }

        private static void InitDevices()
        {
            if (!(Context.Devices.Any()))
            {
                Context.Devices.AddRange(
                    new Device
                    {
                        Name = "web",
                        UserId = Context.Users.FirstOrDefault().Id
                    });
            }
        }
    }
}