﻿namespace ProjectT.DLL.Entities.Enums
{
    public partial class Enums
    {
        enum DeviceType
        {
            Web = 0,
            Windows = 1,
            IOS = 2,
            Android = 3,
            Raspberry = 4
        }
    }
}