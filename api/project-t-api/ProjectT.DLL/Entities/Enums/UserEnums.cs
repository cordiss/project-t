﻿namespace ProjectT.DLL.Entities.Enums
{
    public partial class EnumsClass
    {
        public enum UserStatus
        {
            All = 0,
            Active = 1,
            Blocked = 2
        }
        
        public enum UserRoles
        {
            User = 0,
            Admin = 1
        }
    }
}