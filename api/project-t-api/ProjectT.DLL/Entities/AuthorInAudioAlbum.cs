﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectT.DLL.Entities
{
    public class AuthorInAudioAlbum
    {
        [Key]
        public long Id { get; set; }

        public long AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public Author Author { get; set; }

        public long AlbumId { get; set; }
        [ForeignKey("AlbumId")]
        public Album Album { get; set; }

        public long AudioId { get; set; }
        [ForeignKey("AudioId")]
        public Audio Audio { get; set; }

        [NotMapped]
        public ICollection<Author> Authors { get; set; }
        
        [NotMapped]
        public ICollection<Album> Albums { get; set; }

        [NotMapped]
        public ICollection<Audio> Audios { get; set; }
    }
}
