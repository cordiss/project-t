﻿using ProjectT.DLL.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectT.DLL.Entities
{
    public class Album : BaseEntity
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string CoverFile { get; set; }

        [NotMapped]
        public Author Author { get; set; }

        [NotMapped]
        public ICollection<Audio> Audios { get; set; }
    }
}
