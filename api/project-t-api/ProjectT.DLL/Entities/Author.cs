﻿using ProjectT.DLL.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectT.DLL.Entities
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        [NotMapped]
        public ICollection<Audio> Audios { get; set; } 

        [NotMapped]
        public ICollection<Album> Albums { get; set; }
    }
}