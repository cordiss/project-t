﻿using ProjectT.DLL.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectT.DLL.Entities
{
    public class Audio : BaseEntity
    {
        public string Title { get; set; }
        public int TrackNumber { get; set; }
        public int Year { get; set; }
        public string File { get; set; }
        
        [NotMapped]
        public Author Author { get; set; }

        [NotMapped]
        public Album Album { get; set; }
    }
}