﻿using Microsoft.AspNetCore.Identity;
using ProjectT.DLL.Entities.Enums;
using System;

namespace ProjectT.DLL.Entities
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }
        public string UserPhoto { get; set; }
        public string Phone { get; set; }
        public DateTime CreationData { get; set; }
        public EnumsClass.UserStatus UserStatus { get; set; }
    }
}