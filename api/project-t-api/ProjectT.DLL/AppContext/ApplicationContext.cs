﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProjectT.DLL.Entities;

namespace ProjectT.DLL.AppContext
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Audio> Audios { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<RefreshTable> RefreshTables { get; set; }
        public DbSet<AuthorInAudioAlbum> AuthorInAudioAlbums { get; set; }

        public ApplicationContext(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}