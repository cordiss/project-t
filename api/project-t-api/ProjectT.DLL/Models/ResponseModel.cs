﻿using ProjectT.DLL.Entities.Base;
using System.Collections.Generic;

namespace ProjectT.DLL.Models
{
    public class ResponseModel<T> where T : BaseEntity
    {
        public IEnumerable<T> Entities { get; set; }
        public int ItemsCount { get; set; }
    }
}