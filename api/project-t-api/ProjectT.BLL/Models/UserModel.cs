﻿using ProjectT.BLL.Models.Base;
using ProjectT.BLL.Models.Enums;

namespace ProjectT.BLL.Models
{
    public class UserModel : BaseModel
    {
        public new string Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string UserPhoto { get; set; }
        public EnumsClass.UserStatus Status { get; set; }
        public EnumsClass.UserRoles Role { get; set; }
    }
}
