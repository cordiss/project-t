﻿using ProjectT.BLL.Models.Base;
using System.Collections.Generic;

namespace ProjectT.BLL.Models
{
    public class AlbumModel : BaseModel
    {
        public int ItemsAmong { get; set; }
        public IEnumerable<AlbumModelItem> AlbumModelItems { get; set; }
    }

    public class AlbumModelItem : BaseModel
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string CoverFile { get; set; }
        public AuthorModelItem Author { get; set; }
        public IEnumerable<AudioModel> Audios { get; set; }
    }
}