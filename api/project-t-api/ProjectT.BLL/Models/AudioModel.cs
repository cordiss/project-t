﻿using ProjectT.BLL.Models.Base;

namespace ProjectT.BLL.Models
{
    public class AudioModel : BaseModel
    {
        public string Title { get; set; }
        public int TrackNumber { get; set; }
        public int Year { get; set; }
        public string File { get; set; }
        public AuthorModelItem Author { get; set; }
        public AlbumModelItem Album { get; set; }
    }
}