﻿using System;
using System.Collections.Generic;

namespace ProjectT.BLL.Models.Base
{
    public class BaseModel
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsRemoved { get; set; }

        public ICollection<string> Errors { get; set; }
    }
}