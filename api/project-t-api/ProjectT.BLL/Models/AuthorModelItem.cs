﻿using ProjectT.BLL.Models.Base;
using System.Collections.Generic;

namespace ProjectT.BLL.Models
{
    public class AuthorModel : BaseModel
    {
        public int ItemsAmong { get; set; }
        public IEnumerable<AuthorModelItem> AuthorModelItems { get; set; }
    }

    public class AuthorModelItem : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public IEnumerable<AudioModel> Audios { get; set; }
        public IEnumerable<AlbumModelItem> Albums { get; set; }
    }
}