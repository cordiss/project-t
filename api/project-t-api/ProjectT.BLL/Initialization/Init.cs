﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectT.DLL.AppContext;
using ProjectT.DLL.Entities;
using ProjectT.DLL.Initialization;
using System.Threading.Tasks;

namespace ProjectT.BLL.Initialization
{
    public static class Init
    {
        public static IConfiguration Configuration;
        public static IServiceCollection Services;

        public static void OnInit()
        {
            InitDAL.Configuration = Configuration;
            InitDAL.Services = Services;
            InitDAL.OnInit();
        }

        public static void OnDatabaseDefaultInit(UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationContext context)
        {
            DatabaseInitializator.UserManager = userManager;
            DatabaseInitializator.RoleManager = roleManager;
            DatabaseInitializator.Context = context;

            Task t = DatabaseInitializator.InitializeRolesAsync();
            t.Wait();
  
            Task f = DatabaseInitializator.InitializeEntitiesAsync();
            f.Wait();
        }
    }
}