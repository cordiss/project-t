﻿using ProjectT.BLL.Extensions.Map.EntityToModel;
using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.CollectionToModel
{
    public static class AlbumMapExtension
    {
        public static IEnumerable<AlbumModelItem> ToModel(this IEnumerable<Album> albums)
        {
            var albumModels = new List<AlbumModelItem>();

            foreach(var album in albums)
            {
                var albumModel = new AlbumModelItem();

                albumModel.Id = album.Id;
                albumModel.Title = album.Title;
                albumModel.Description = album.Description;
                albumModel.CoverFile = album.CoverFile;
                albumModel.Year = album.Year;
                albumModel.CreationDate = album.CreationData;
                albumModel.IsRemoved = album.IsRemoved;

                albumModel.Author = album.Author.ToModel(null, null);
                albumModel.Audios = album.Audios.ToModel(null, null);

                albumModels.Add(albumModel);
            }

            return albumModels;
        }
    }
}