﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.CollectionToModel
{
    public static class AudioMapExtension
    {
        public static IEnumerable<AudioModel> ToModel(this IEnumerable<Audio> audios, 
            AuthorModelItem mappedAuthorModel, 
            AlbumModelItem mappedAlbumModel)
        {
            var audioModels = new List<AudioModel>();

            foreach(var audio in audios)
            {
                var audioModel = new AudioModel();

                audioModel.Id = audio.Id;
                audioModel.Title = audio.Title;
                audioModel.TrackNumber = audio.TrackNumber;
                audioModel.Year = audio.Year;
                audioModel.File = audio.File;
                audioModel.Author = mappedAuthorModel;
                audioModel.Album = mappedAlbumModel;
                audioModel.CreationDate = audio.CreationData;
                audioModel.IsRemoved = audio.IsRemoved;

                audioModels.Add(audioModel);
            }

            return audioModels;
        }
    }
}