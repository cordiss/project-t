﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.CollectionToModel
{
    public static class AuthorMapExtension
    {
        public static IEnumerable<AuthorModelItem> ToModel(
            this IEnumerable<Author> authors)
        {
            var authorModels = new List<AuthorModelItem>();

            foreach(var author in authors)
            {
                var authorModel = new AuthorModelItem();

                authorModel.Id = author.Id;
                authorModel.Name = author.Name;
                authorModel.Description = author.Description;
                authorModel.CreationDate = author.CreationData;
                authorModel.IsRemoved = author.IsRemoved;
                authorModel.Albums = mappedAlbumModels;
                authorModel.Audios = author.Audios.ToModel(null, null);

                authorModels.Add(authorModel);
            }

            return authorModels;
        }
    }
}