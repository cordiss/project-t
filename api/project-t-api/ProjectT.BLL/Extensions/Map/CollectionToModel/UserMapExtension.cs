﻿using ProjectT.BLL.Models;
using ProjectT.BLL.Models.Enums;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.CollectionToModel
{
    public static class UserMapExtension
    {
        public static IEnumerable<UserModel> ToModel(this IEnumerable<User> users, EnumsClass.UserRoles userRole)
        {
            var userModels = new List<UserModel>();

            foreach(var user in users)
            {
                var userModel = new UserModel();

                userModel.Id = user.Id;
                userModel.FullName = user.FullName;
                userModel.Phone = user.Phone;
                userModel.UserPhoto = user.UserPhoto;
                userModel.Role = userRole;
                userModel.Status = (EnumsClass.UserStatus)user.UserStatus;

                userModels.Add(userModel);
            }

            return userModels;
        }
    }
}