﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;

namespace ProjectT.BLL.Extensions.Map.EntityToModel
{
    public static class AudioMapExtension
    {
        public static AudioModel ToModel(
           this Audio audio, 
           AuthorModelItem mappedAuthorModel,
           AlbumModelItem mappedAlbumModel)
        {
            var audioModel = new AudioModel();

            audioModel.Id = audio.Id;
            audioModel.Title = audio.Title;
            audioModel.TrackNumber = audio.TrackNumber;
            audioModel.Year = audio.Year;
            audioModel.File = audio.File;
            audioModel.Author = mappedAuthorModel;
            audioModel.Album = mappedAlbumModel;
            audioModel.CreationDate = audio.CreationData;
            audioModel.IsRemoved = audio.IsRemoved;

            return audioModel;
        }
    }
}