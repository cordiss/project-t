﻿using ProjectT.BLL.Extensions.Map.CollectionToModel;
using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;

namespace ProjectT.BLL.Extensions.Map.EntityToModel
{
    public static class AlbumMapExtension
    {
        public static AlbumModelItem ToModel(this Album album)
        {
            var albumModel = new AlbumModelItem();

            albumModel.Id = album.Id;
            albumModel.Title = album.Title;
            albumModel.Description = album.Description;
            albumModel.CoverFile = album.CoverFile;
            albumModel.Year = album.Year;
            albumModel.CreationDate = album.CreationData;
            albumModel.IsRemoved = album.IsRemoved;

            albumModel.Author = album.Author.ToModel(null, null);
            albumModel.Audios = album.Audios.ToModel(null, null);

            return albumModel;
        }
    }
}