﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.EntityToModel
{
    public static class AuthorMapExtension
    {
        public static AuthorModelItem ToModel(
            this Author author, 
            IEnumerable<AlbumModelItem> mappedAlbumModels,
            IEnumerable<AudioModel> mappedAudioModels)
        {
            var authorModel = new AuthorModelItem();

            authorModel.Id = author.Id;
            authorModel.Name = author.Name;
            authorModel.Description = author.Description;
            authorModel.CreationDate = author.CreationData;
            authorModel.IsRemoved = author.IsRemoved;
            authorModel.Albums = mappedAlbumModels;
            authorModel.Audios = mappedAudioModels;

            return authorModel;
        }
    }
}