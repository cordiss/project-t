﻿using ProjectT.BLL.Models;
using ProjectT.BLL.Models.Enums;
using ProjectT.DLL.Entities;

namespace ProjectT.BLL.Extensions.Map.EntityToModel
{
    public static class UserMapExtension
    {
        public static UserModel ToModel(this User user, EnumsClass.UserRoles userRole)
        {
            var userModel = new UserModel();

            userModel.Id = user.Id;
            userModel.FullName = user.FullName;
            userModel.Phone = user.Phone;
            userModel.UserPhoto = user.UserPhoto;
            userModel.Role = userRole;
            userModel.Status = (EnumsClass.UserStatus)user.UserStatus;

            return userModel;
        }
    }
}