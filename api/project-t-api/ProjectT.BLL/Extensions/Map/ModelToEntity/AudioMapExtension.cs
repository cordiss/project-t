﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;

namespace ProjectT.BLL.Extensions.Map.ModelToEntity
{
    public static class AudioMapExtension
    {
        public static Audio ToEntity(this AudioModel audioModel, Author mappedAuthor, Album mappedAlbum)
        {
            var audio = new Audio();

            audio.Id = audioModel.Id;
            audio.Title = audioModel.Title;
            audio.TrackNumber = audioModel.TrackNumber;
            audio.Year = audioModel.Year;
            audio.File = audioModel.File;
            audio.Author = mappedAuthor;
            audio.Album = mappedAlbum;
            audio.CreationData = audioModel.CreationDate;
            audio.IsRemoved = audioModel.IsRemoved;

            return audio;
        }
    }
}