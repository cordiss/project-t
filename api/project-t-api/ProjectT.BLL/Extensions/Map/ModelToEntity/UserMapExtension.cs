﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using ProjectT.DLL.Entities.Enums;

namespace ProjectT.BLL.Extensions.Map.ModelToEntity
{
    public static class UserMapExtension
    {
        public static User ToEntity(this UserModel userModel)
        {
            var user = new User();

            user.FullName = userModel.FullName;
            user.Phone = userModel.Phone;
            user.UserPhoto = userModel.UserPhoto;
            user.UserStatus = (EnumsClass.UserStatus)userModel.Status;

            return user;
        }
    }
}