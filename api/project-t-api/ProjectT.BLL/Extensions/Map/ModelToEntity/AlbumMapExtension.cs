﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.ModelToEntity
{
    public static class AlbumMapExtension
    {
        public static Album ToEntity(this AlbumModelItem albumModel, List<Audio> audios)
        {
            var album = new Album();

            album.Id = albumModel.Id;
            album.Title = albumModel.Title;
            album.Description = albumModel.Description;
            album.CoverFile = albumModel.CoverFile;
            album.Year = albumModel.Year;
            album.CreationData = albumModel.CreationDate;
            album.IsRemoved = albumModel.IsRemoved;

            album.Author = albumModel.Author.ToEntity(null, null);
            album.Audios = audios;

            return album;
        }
    }
}