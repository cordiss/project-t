﻿using ProjectT.BLL.Models;
using ProjectT.DLL.Entities;
using System.Collections.Generic;

namespace ProjectT.BLL.Extensions.Map.ModelToEntity
{
    public static class AuthorMapExtension
    {
        public static Author ToEntity(
            this AuthorModelItem authorModel, 
            List<Album> albums, 
            List<Audio> audios)
        {
            var author = new Author();

            author.Id = authorModel.Id;
            author.Name = authorModel.Name;
            author.Description = authorModel.Description;
            author.CreationData = authorModel.CreationDate;
            author.IsRemoved = authorModel.IsRemoved;
            author.Albums = albums;
            author.Audios = audios;

            return author;
        }
    }
}