﻿using ProjectT.BLL.Models;
using ProjectT.BLL.Services.Interfaces;
using ProjectT.DLL.Repositories.Interfaces;
using System.Threading.Tasks;
using ProjectT.BLL.Extensions.Map.CollectionToModel;
using ProjectT.BLL.Extensions.Map.ModelToEntity;
using ProjectT.BLL.Extensions.Map.EntityToModel;

namespace ProjectT.BLL.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorInAudioAlbumRepository _authorInAudioAlbumRepository;

        public AuthorService(IAuthorInAudioAlbumRepository authorInAudioAlbumRepository)
        {
            _authorInAudioAlbumRepository = authorInAudioAlbumRepository;
        }

        public async Task<AuthorModel> GetAllAsync()
        {
            var authorModel = new AuthorModel();

            var result = await _authorInAudioAlbumRepository.GetAuthorsAsync();

            authorModel.AuthorModelItems = result.Entities.ToModel();
            authorModel.ItemsAmong = result.ItemsCount;
            return authorModel;
        }
        
        public async Task<AuthorModelItem> GetByIdAsync(long id)
        {
            var result = await _authorInAudioAlbumRepository.GetAuthorByIdAsync(id);

            var albumModels = result.Albums.ToModel();
            var audioModels = result.Audios.ToModel(mappedAuthorModel: null, mappedAlbumModel: null);

            var authorModel = result.ToModel(albumModels, audioModels);

            return authorModel;
        }
    }
}