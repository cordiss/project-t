﻿using ProjectT.BLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectT.BLL.Services.Interfaces
{
    public interface IAuthorService
    {
        Task<AuthorModel> GetAllAsync();
        Task<AuthorModelItem> GetByIdAsync(long id);
    }
}