﻿using ProjectT.BLL.Models;
using System.Threading.Tasks;

namespace ProjectT.BLL.Services.Interfaces
{
    public interface IAlbumService
    {
        Task<AlbumModel> GetAllAsync();
        Task<AlbumModelItem> GetByIdAsync(long id);
    }
}