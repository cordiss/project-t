﻿using ProjectT.BLL.Extensions.Map.CollectionToModel;
using ProjectT.BLL.Extensions.Map.EntityToModel;
using ProjectT.BLL.Models;
using ProjectT.BLL.Services.Interfaces;
using ProjectT.DLL.Repositories.Interfaces;
using System.Threading.Tasks;

namespace ProjectT.BLL.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly IAuthorInAudioAlbumRepository _authorInAudioAlbumRepository;
        public AlbumService(IAuthorInAudioAlbumRepository authorInAudioAlbumRepository)

        {
            _authorInAudioAlbumRepository = authorInAudioAlbumRepository;
        }

        public async Task<AlbumModel> GetAllAsync()
        {
            var albumModel = new AlbumModel();

            var result = await _authorInAudioAlbumRepository.GetAlbumsAsync();
            albumModel.AlbumModelItems = result.Entities.ToModel();
            albumModel.ItemsAmong = result.ItemsCount;            
            return albumModel;
        }

        public async Task<AlbumModelItem> GetByIdAsync(long id)
        {
            var result = await _authorInAudioAlbumRepository.GetAlbumByIdAsync(id);

            var albumModel = result.ToModel();
            return albumModel;
        }
    }
}